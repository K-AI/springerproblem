# Springerproblem

## Building

Compile with optimization, using the C99 standard and displaying all warnings:

`gcc main.c -o out -O3 --std=c99 -lm -Wall -Werror`
